﻿using mediatorMicroService.Models;
using mediatorMicroService.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace mediatorMicroService.Handlers
{
    public class GetAllUsersHandler : IRequestHandler<GetAllUserQuery, List<user>>
    {
        private readonly UserContext _context;

        public GetAllUsersHandler(UserContext context)
        {
            _context = context;
        }

        public async Task<List<user>> Handle(GetAllUserQuery request, CancellationToken cancellationToken)
        {
          return  await _context.Users.ToListAsync();
        }
    }
}
